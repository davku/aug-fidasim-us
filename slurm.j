#!/bin/bash -l
##################################################################
#
# A simple submit script for starting mpi jobs on two nodes 
#
# @author Rechenzentrum der MPG, Garching
# @author Germany
#
##################################################################
# Standard output and error:
#SBATCH -o ./%x.out.%j
#SBATCH -e ./%x.err.%j
#
# Initial working directory
#SBATCH -D ./
#
# Job Name:
#SBATCH -J fidasim
#
# Queue/Partition
#SBATCH --partition=p.tok.openmp
#
# Quality of Service
#SBATCH --qos=p.tok.openmp.2h
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#
#CPUs per task for OpenMP
#SBATCH --cpus-per-task=32
#
#Set memory requirement (default: 1 GB, max: 5 GB)
#SBATCH --mem 185GB
#
#Wall clock limit
#SBATCH --time=2:00:00
#
#SBATCH --mail-type=all
#SBATCH --mail-user=$USER@ipp.mpg.de

export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK:-1}
# For pinning threads correctly
export OMP_PLACES=cores

srun /u/davku/src/FIDASIM/fidasim RUNID_inputs.dat

# end of job script
###################################################################
