# Running FIDASIM from TRANSP/NUBEAM on the TOK cluster


## Install FIDASIM

Log in to the TOK cluster e.g.:

	ssh toki01.bc.rzg.mpg.de

Load these modules for a general setup and for compiling

	module load trview impi/2021.9 openmpi/4.1

follow the instructions here to install FIDASIM: https://d3denergetic.github.io/FIDASIM/page/01_getting_started/01_install.html

Use the gfortran compiler (set through environment variables) or disable the mkl flag as there are issues with linking.

You can copy the atomic tables over from 

	/shares/departments/AUG/users/davku/pub
	
To i.e. 

    /u/USER/pub/atomic_tables.h5

Which will save you some time generating them yourself (these are just the default tables as described in the instructions)


## Set up scripts

Put the following lines in your RC file on TOK, i.e. 

    nano ~/.bashrc
    
adjust the paths and paste them to the file:

    export PYTHONPATH=/path/to/aug-fidasim-us:$PYTHONPATH
    alias newfidasim='python /path/to/aug-fidasim-us/run_aug_nubeam.py'


## FIDASIM python script fixes:

in lib/python/fidasim/preprocessing.py, replace line 426

    if nbi['ashape'] not in [1, 2]:
    
    by the following

        for i in nbi['ashape']:
            if i not in [1, 2]:

## Set up a FIDASIM run from TRANSP/NUBEAM:

(put these lines in your RC file if you dont want to repeat them in each session):

	module purge
	module load anaconda/3/2023.03 aug_sfutils/tok/0.8.0 

Copy over the TRANSP run as the batch jobs dont have access to the shares:

	cp -r /shares/departments/AUG/users/davku/tr_client/AUGD/38507 .

Set up FIDASIM run from within the directory you just copied to:	
	
	newfidasim . -i 3 -r 2
	
This will create a FIDASIM run based on the fi_3.cdf FBM file with a runid appended by _2 for distinction. The output file of FIDASIM will be in the folder from which you issue the command

## Submit the job on tok

copy the slurm.j file in this directory to the one where you created the run. Replace the RUNID_inputs.dat filename with the one applicable to you (should have been shown at the end of the preprocessing in the previous step).

Then issue

    sbatch slurm.j
    
and the job should be queued. you can check with 

    squeue -u $USER
    
and you will also get emails about the job
