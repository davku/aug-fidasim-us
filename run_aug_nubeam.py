#!/usr/bin/env python

import aug_fidasim as af
import argparse
import numpy as np
from scipy.interpolate import interp1d
import fidasim as fs
import fidasim.utils as fu
import fidasim.preprocessing as fp
import os
import re
import warnings



def auto_populate_variables():
    """
    Automatically populates the shot, runid, and cdf_ids variables based on the current working directory and its contents.
    """
    # Get the current working directory
    cwd = os.getcwd()

    # Extract shot and runid from the directory name
    parts = cwd.split('/')
    #print(parts)
    if len(parts) < 4 or parts[-3] != 'AUGD':
        raise ValueError("Current directory path does not conform to the expected format.")

    shot, runid = parts[-2], parts[-1]

    # Define the regex pattern for matching .cdf files
    cdf_pattern = re.compile(rf"{shot}{runid}_fi_(\d+)\.cdf")
    cdf_ids = []

    # Scan the directory for matching files
    try:
        for file in os.listdir(cwd):
            match = cdf_pattern.match(file)
            if match:
                cdf_ids.append(int(match.group(1)))
    except Exception as e:
        raise RuntimeError(f"Error while scanning the directory: {e}")

    # Handle the case where no matching files are found
    if not cdf_ids:
        raise FileNotFoundError(f"No .cdf files found matching the pattern {shot}{runid}_fi_k.cdf")

    return int(shot), runid, np.array(cdf_ids)

# Commenting out the function call to prevent execution
# shot, runid, cdf_ids = auto_populate_variables()
# print(f"Shot: {shot}, RunID: {runid}, CDF IDs: {cdf_ids}")


def run_test(args):
    #fida_dir = fs.utils.get_fidasim_dir()
    #test_dir = fida_dir + '/test'

    #shot = 38581
    #runid = "A06"
    #cdf_ids = np.array([1, 2, 3,4])
    shot, runid, cdf_ids = auto_populate_variables()
    cdf_ids=np.sort(cdf_ids)
    #print(cdf_ids)
    out_dir = args.path#"%%FIDASIM_PATH%%"
    i=int(args.cdf_id)
    #print(i)
    #transp_dir =  "/shares/users/work/davku/tr_client/AUGD/" + str(shot) + "/" + runid 
    transp_dir=os.getcwd()
    transp_id = "%d%s" %(shot,runid)
    transp_cdf = transp_dir + "/" + transp_id  + ".CDF"
    #trdat = transp_dir + "/" + transp_id + "TR.DAT"
    
    rmin = 100
    rmax = 220
    nr = 100
    zmin = -100
    zmax = 100
    nz = 100
    phimin = 0
    phimax = 2*np.pi
    nphi = 0
    
    grid = fu.rz_grid(rmin,rmax,nr,zmin,zmax,nz)
    
    nubeam_distribution_file = transp_dir + "/" + transp_id + "_fi_" + str(cdf_ids[i]) + ".cdf"
    f = fu.read_nubeam(nubeam_distribution_file, grid, btipsign=-1)    
    time = f["time"]
    timestr = "%05d" %(time*1000)
    #fidasim_runid_transp = "NUMBER_%d%s-%s" %(shot,runid,timestr)
    #fidasim_run_transp = "%s/%s_inputs.dat" %(out_dir,fidasim_runid_transp)

    #geqdsk = ("%s/g%d.%s") %(transp_dir,shot,timestr)
    if args.eqdsk_name == '0':
        geqdsk = ("%1.3f.eqdsk") % (time)
        fidasim_runid = ("%s_%s") % (transp_id, geqdsk)
        if args.runid:
            fidasim_runid = ("%s_%s") % (fidasim_runid, args.runid)
        geqdsk = ("%s/%d_%s") % (transp_dir, shot, geqdsk)        
    else:
        geqdsk=args.eqdsk_name
        fidasim_runid = ("%s_%s") % (transp_id, geqdsk)
        if args.runid:
            fidasim_runid = ("%s_%s") % (fidasim_runid, args.runid)


    fields, rho, btipsign = fu.read_geqdsk(geqdsk, grid, poloidal=False)
    #warnings.filterwarnings('error',category=RuntimeWarning)
    #try:
    #    fields, rho, btipsign = fu.read_geqdsk(geqdsk, grid, poloidal=False)
    #except Warning as e:
    #    print(f"Debug message: An error occurred - {e}. Trying with poloidal=False.")
    #    fields, rho, btipsign = fu.read_geqdsk(geqdsk, grid, poloidal=False)

    
    #fields, rho, btipsign = fu.read_geqdsk(geqdsk,grid, poloidal = True)
    plasma = fu.extract_transp_plasma(transp_cdf,time,grid,rho)

    [mass, fractions, pinj, einj] = af.get_nbi_params(shot, time)
    inputs = af.make_inputs(shot,time,fidasim_runid,out_dir,mass, fractions,pinj, einj)
    #print(inputs)
    
    spec = af.get_los_for_shot(shot)

#    nbi = af.nbi_std_shifted.copy()
    nbi = af.nbi_38507A03.copy()
    bgrid = fu.beam_grid(nbi,300.0, length=180.0)
    
    #bgrid['nx'] = int(bgrid['nx']/2) #Substantial acceleration
    #bgrid['ny'] = int(bgrid['ny']/2)
    #bgrid['nz'] = int(bgrid['nz']/2)
    
    inputs.update(bgrid.copy())
    

    #print(rho)
    #print(plasma)

    #nbi = fu.nubeam_geometry(nubeam)
    fp.prefida(inputs, grid, nbi, plasma, fields, f, spec=spec)

    return 0

def main():
    parser = argparse.ArgumentParser(description="Prepares a FIDASIM run from NUBEAM")

    parser.add_argument('path', help='Result directory')
    parser.add_argument('-r', '--runid', default = '', help='Runid')
    parser.add_argument('-i', '--cdf_id', default = '0', help='FBM ID to use')
    parser.add_argument('-e', '--eqdsk_name', default = '0', help='Specific eqdsk file')

    args = parser.parse_args()

    run_test(args)

if __name__=='__main__':
    main()
