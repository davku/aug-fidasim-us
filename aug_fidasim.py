# aug_fidasim.py

# Import any required modules
import fidasim.utils as fu
import fidasim.preprocessing as fp
import numpy as np
from numpy import array
import os
import h5py as h5
import matplotlib.pyplot as plt
import os
import pandas as pd
from scipy import signal
import IPython.display as ipd
import aug_sfutils as sf
import argparse
from scipy.interpolate import interp1d
from os.path import exists

# Define your functions here

def shot_to_year(shot):
    """
    Convert shot number to year.

    Parameters:
    shot (int): Discharge number

    Returns:
    int: Year of campaign if found, -1 otherwise
    """
    sprd_start = '/shares/departments/AUG/users/sprd'
    file_path = f'{sprd_start}/CALIB/SHOT_TO_YEAR'

    try:
        with open(file_path, 'r') as file:
            for line in file:
                sh, year = map(int, line.split())
                if sh > shot:
                    return str(year)
    except IOError:
        # Error handling if file can't be opened or read
        pass

    print(f'Year not found for shot={shot[0]}!')
    return -1


def get_los_for_shot(nshot):
    los_path = '/shares/departments/AUG/users/sprd/loscoord/'
    prefix = 'LOS_COORD_'
    #cez = sf.SFREAD(nshot,'CFR') #Get year
    #if cez.status:
    #    par = cez.getparset('PARAM')
    #else:
    #    print('Shotfile broken!')
    #year = str(cez.time)[0:4] 
    year=shot_to_year(nshot)
    chanstr = ('CHAN_%02d'%2)
    li = np.array([])

    cez = sf.SFREAD(nshot,'CFR') #Get year
    if cez.status:
        par = cez.getparset('PARAM')
        print('CER Parameters loaded!')
    else:
        print('Shotfile broken!')

    print('Collecting LOS Names of year %s for selection:'%year)
    
    
    for i in range(2,100):
        chanstr = ('CHAN_%02d'%i)
        try:
            li = np.append(li,par[chanstr])
            if not li[-1].decode('UTF-8').strip()[-1].isdigit():
                li = li[0:-1]; #Remove last wrong element
                break
        except:
            print('No channel %s found - stopping iteration'%chanstr)
            break


    li = [i.decode('UTF-8').strip() for i in li]
    #print(li)

    filename = los_path + prefix + year
    while (not exists(filename)):
        year = str(int(year)-1)
        filename = los_path + prefix + year
        
    print('Collecting LOS data of %s'%year)
    df = pd.read_table(filename, sep='\'',header=None, skipinitialspace =True, skiprows=2, engine='python')
    df1 = df[df.columns[1]].astype('str')
    df2 = df[df.columns[2]].str.split('[ ]{1,}',expand=True) #split at one or more! whitespaces
    for col in range(0,6):
        df2[df2.columns[col]]=pd.to_numeric(df2[df2.columns[col]])

    df1.astype('str').apply(str)

    df2.columns = ['R1', 'PHI1', 'Z1', 'R2', 'PHI2', 'Z2','not']
    df2.pop('not')
    df2.set_index(df1.apply(str), inplace=True)

    sind = lambda degrees: np.sin(np.deg2rad(degrees))
    cosd = lambda degrees: np.cos(np.deg2rad(degrees))


    df2['X1'] = df2['R1'] * df2['PHI1'].apply(cosd) *100.0
    df2['Y1'] = df2['R1'] * df2['PHI1'].apply(sind) *100.0
    df2['X2'] = df2['R2'] * df2['PHI2'].apply(cosd) *100.0
    df2['Y2'] = df2['R2'] * df2['PHI2'].apply(sind) *100.0
    df2['Z1'] = df2['Z1'] * 100.0
    df2['Z2'] = df2['Z2'] * 100.0

    df2['A1'] = df2['X2'] - df2['X1']
    df2['A2'] = df2['Y2'] - df2['Y1']
    df2['A3'] = df2['Z2'] - df2['Z1']

    df2['norm'] = (df2['A1']*df2['A1']) + (df2['A2']*df2['A2']) + (df2['A3']*df2['A3'])
    df2['norm']=df2['norm'].apply(np.sqrt)

    df2['A1'] = df2['A1'] / df2['norm']
    df2['A2'] = df2['A2'] / df2['norm']
    df2['A3'] = df2['A3'] / df2['norm']

    df2['radius'] = df2['X2'] * df2['X2'] + df2['Y2'] * df2['Y2']
    df2['radius'] = df2['radius'].apply(np.sqrt)

    #print(df2.index)
    df2.index = df2.index.str.strip() #Remove white spaces in index for sorting
    nchan = len(li)
    #print(li)
    #print(df2)
    spec_string = '|'.join(li)
    spec_string_bytes = [i.encode('UTF-8') for i in li]

    check = np.transpose(df2.query('index in @li').reindex(li)[['R1','PHI1','Z1','R2','PHI2','Z2']].values)
    xyz = np.transpose(df2.query('index in @li').reindex(li)[['X1','Y1','Z1']].values)
    axis = np.transpose(df2.query('index in @li').reindex(li)[['A1','A2','A3']].values)
    radius=np.transpose(df2.query('index in @li').reindex(li)[['radius']].values)
    los_names = df2.query('index in @li').reindex(li).index.tolist()
    los_names = [i.encode('UTF-8') for i in los_names]
    #print(los_names[19])    
    #print(check[:,19])
    #print(los_names)
    
    if (nshot > 27500):
        sigma_pi = np.ones(nchan)*0.7
        index = np.where(xyz[2,:]>100.)
        sigma_pi[index] = 1.
    else:
        sigma_pi = np.ones(nchan)*0.5
        
    spec = {
    "nchan": nchan,
    "system": "AUG FIDA",
    "data_source": "measurement",
    "id":np.array(los_names, dtype='|S32'),
    "radius":np.squeeze(radius),
    "lens":xyz,
    "axis":axis,
    "spot_size":np.ones(nchan)*0.0,
    "sigma_pi":sigma_pi
    }
    return spec#[xyz, axis, radius, los_names, sigma_pi]

def get_nbi_params(nshot,time):
    print('Getting NBI parameters for shot %05d, time %1.2f'%(nshot,time))
    #CAREFUL WHEN SPECIES ARE MIXED! (NEEDS TO BE CHANGED)
    nis = sf.SFREAD(nshot,'NIS') #Get NBI Info 
    if nis.status:
        par = nis.getparset('INJ1')
        print('NBI Shotfile loaded!')
    else:
        print('Shotfile broken!')

    mass=np.float64(par['M'])
    #print(mass)
    fractions = np.array([0.46, 0.369, 0.171])
    einj = np.float64(par['UEXQ'][2]) #Energy of source 3
   #print(par['UEXQ'])

    pniq = nis.getobject('PNIQ')
    tpniq = nis.gettimebase('PNIQ')
    time_ind = np.argmin(np.abs(tpniq-time))
    pinj = np.float64(pniq[time_ind,2,0])/1e6
    #print(pinj)

        
    return [mass,  fractions,pinj, einj]

def make_inputs(nshot,time,runid,out_dir,mass, fractions,pinj, einj):
    print('Making inputs of run %s'%runid)
    inputs = {
        "shot" : nshot,
        "time" : time,
        "runid" : runid,
        "device": "ASDEX Upgrade",
        "comment" : "initialized from aug_fidasim.py",
        "result_dir" : out_dir,
        "tables_file" : "/shares/departments/AUG/users/davku/pub/atomic_tables.h5",
        "calc_bes":1,
        "calc_dcx":1,
        "calc_halo":1,
        "calc_cold":1,
        "calc_brems":1,
        "calc_fida":1,
        "calc_npa":0,
        "calc_pfida":0,
        "calc_pnpa":0,
        "calc_neutron":0,
        "calc_birth":0,
        "calc_fida_wght":1,
        "calc_npa_wght":0,
        "n_fida":5000000,
        "n_pfida":50000000,
        "n_npa":5000000,
        "n_pnpa":50000000,
        "n_nbi":50000,
        "n_halo":50000,
        "n_dcx":500000,
        "n_birth":10000,
        #"n_fida":500000,
        #"n_pfida":50000000,
        #"n_npa":5000000,
        #"n_pnpa":50000000,
        #"n_nbi":5000,
        #"n_halo":500,
        #"n_dcx":50000,
        #"n_birth":1000,
        "ab":mass,
        "pinj":pinj,#2.466,
        "einj":einj,#58.58,
        "current_fractions": fractions,#np.array([0.288, 0.353, 0.359]),#np.array([0.46, 0.369, 0.171]), #CHECK!!!!
        "ai":mass,
        #"ab":1.0,
        #"pinj":1.836,#2.466,
        #"einj":54.23,#58.58,
        #"current_fractions": np.array([0.46, 0.369, 0.171]),#np.array([0.288, 0.353, 0.359]),#np.array([0.46, 0.369, 0.171]), #CHECK!!!!
        #"ai":1.0,
        "impurity_charge":5,
        "nlambda":1024,
        "lambdamin":645.0,
        "lambdamax":669.0,
        #"nlambda":32,
        #"lambdamin":655.0,
        #"lambdamax":659.0,
        "ne_wght":10,
        "np_wght":10,
        "nphi_wght":8,
        "emax_wght":100.0,
        "nlambda_wght":10,
        "lambdamin_wght":647.0,
        "lambdamax_wght":665.0
        }
    return inputs    

# Add as many functions as you need

# Optionally define some constants or global variables
nubeam = {
    "NAME": "AUG BEAM 3",
    "NBSHAP": 1,
    "FOCLZ": 850.0000,
    "FOCLR": 650.0000,
    "DIVZ":0.0099,
    "DIVR":0.0099,
    "BMWIDZ":25.0000,
    "BMWIDR":11.0000,
    "RTCENA":94.0390,
    "XLBTNA":923.4400,
    "XBZETA":312.8894,
    "XYBSCA":-60.0000,
    "NLJCCW":1, #Orientation of Ip. 1 for True/Counter-clockwise current, 0 or -1 for False/Clock-wise current
    "NLCO" : 1, #1 for Co-beam, 0 or -1 for Counter-beam
    "NBAPSHA":np.array([1,1]),
    "XLBAPA":np.array([587.3427, 667.9514]),
    "XYBAPA":np.array([-9.9700,-9.9700]),
    "RAPEDGA":np.array([30.0000, 30.0000]),
    "XZPEDGA":np.array([40.4920,48.2250 ]),
    "XRAPOFFA":np.array([19.4380,-18.9290]),
    "XZAPOFFA":np.array([9.2160,0.3740]),
}

nbi_std_shifted={'data_source': 'TRANSP/NUBEAM namelist, shifted eq. of one channel',
    'name': 'AUG BEAM 3', 
    'shape': 1, 
    'src': np.array([ 629.45775323, -677.62857735,  -60.        ]), 
    'axis': np.array([-0.585671331963931,0.806310935982928,0.082774974546612]), 
    'focy': 650.0, 
    'focz': 850.0, 
    'divy': np.array([0.0099, 0.0099, 0.0099]), 
    'divz': np.array([0.0099, 0.0099, 0.0099]), 
    'widy': 11.0, 
    'widz': 25.0, 
    'naperture': 2, 
    'ashape': array([1, 1]), 
    'awidy': np.array([30., 30.]), 
    'awidz': np.array([40.492, 48.225]), 
    'aoffy': np.array([ 19.438, -18.929]), 
    'aoffz': np.array([9.216, 0.374]), 
    'adist': np.array([587.3427, 667.9514])}
    
nbi_std={'data_source': 'TRANSP/NUBEAM namelist',
    'name': 'AUG BEAM 3', 
    'shape': 1, 
    'src': np.array([ 629.45775323, -677.62857735,  -60.        ]), 
    'axis': np.array([-0.60037239,  0.79517125,  0.08518025]), 
    'focy': 650.0, 
    'focz': 850.0, 
    'divy': np.array([0.0099, 0.0099, 0.0099]), 
    'divz': np.array([0.0099, 0.0099, 0.0099]), 
    'widy': 11.0, 
    'widz': 25.0, 
    'naperture': 2, 
    'ashape': array([1, 1]), 
    'awidy': np.array([30., 30.]), 
    'awidz': np.array([40.492, 48.225]), 
    'aoffy': np.array([ 19.438, -18.929]), 
    'aoffz': np.array([9.216, 0.374]), 
    'adist': np.array([587.3427, 667.9514])}

  
nbi_opt = {'data_source': "TRANSP/NUBEAM namelist, direct, shifted eq. of one channel",
       'name': 'AUG BEAM 3', 
       'shape': 1, 
       'src': np.array([629.45775323, -677.62857735,  -60.        ]), 
       'axis': np.array([-0.597178154999978,0.797587493075150,0.085043765679320]),       
       'focy': 650.0, 
       'focz': 850.0, 
       'divy': np.array([0.0099, 0.0099, 0.0099]), 
       'divz': np.array([0.0099, 0.0099, 0.0099]), 
       'widy': 11.0, 
       'widz': 25.0, 
       'naperture': 2, 
       'ashape': np.array([1, 1]), 
       'awidy': np.array([30., 30.]), 
       'awidz': np.array([40.492, 40.492]), 
       'aoffy': np.array([ 19.438, -18.929]), 
       'aoffz': np.array([9.216, 0.374]), 
       'adist': np.array([587.3427, 667.9514])}
       
nbi_38507A03 = {'data_source': "Anton",
       'name': 'AUG BEAM 3', 
       'shape': 1, 
       'src': np.array([629.45795641214808, -677.62838860865449, -60.00000000000000]), 
       'axis': np.array([ -0.6003726281140984, 0.7951710708064581, 0.0851802533682635]), 
       'focy': 650.0, 
       'focz': 850.0, 
       'divy': np.array([0.0099, 0.0099, 0.0099]), 
       'divz': np.array([0.0099, 0.0099, 0.0099]), 
       'widy': 11.0, 
       'widz': 25.0, 
       'naperture': 2, 
       'ashape': array([1, 1]), 
       'awidy': np.array([30., 30.]), 
       'awidz': np.array([40.492, 48.225]), 
       'aoffy': np.array([ 19.438, -18.929]), 
       'aoffz': np.array([9.216, 0.374]), 
       'adist': np.array([587.3427, 667.9514])}       
       
              
nbi_vur = {'data_source': "Anton",
       'name': 'AUG BEAM 3', 
       'shape': 1, 
       'src': np.array([632.9701260742356,-674.5717765302874,-60.000095516443250 ]), 
       'axis': np.array([-0.606732177856547,0.790300266086371,0.085449129763053]), 
       'focy': 650.0, 
       'focz': 850.0, 
       'divy': np.array([0.009899999946356,0.010499999858439,0.012299999594688]), 
       'divz': np.array([0.009899999946356,0.010499999858439,0.012299999594688]), 
       'widy': 11.0, 
       'widz': 25.0, 
       'naperture': 2, 
       'ashape': np.array([1, 1]), 
       'awidy': np.array([30., 30.]), 
       'awidz': np.array([40., 40.]), 
       'aoffy': np.array([19.488661870520612,-18.224932609065252]), 
       'aoffz': np.array([9.102938998696523,0.186546777457401]), 
       'adist': np.array([5.861086952377470e+02,6.677809604002308e+02])}

# If needed, include code that should only run when the module is not imported
if __name__ == "__main__":
    # Code to execute when the module is run directly
    pass
